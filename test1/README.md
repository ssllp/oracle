姓名：佘尚联 学号：202010414316

# 实验1：SQL语句的执行计划分析与优化指导

## 实验目的

  分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

## 实验数据库和用户

  数据库是pdborcl，用户是sys和hr

## 实验内容

- 对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
- 设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。

## 实验步骤

- 用户hr默认没有统计权限，打开统计信息功能autotrace时要报错，必须要向用户hr授予以下视图的选择权限：

```text
 v_$sesstat, v_$statname 和 v_$session
```

- 权限分配过程如下

```sql
$ sqlplus sys/123@localhost/pdborcl as sysdba
@$ORACLE_HOME/sqlplus/admin/plustrce.sql
create role plustrace;
GRANT SELECT ON v_$sesstat TO plustrace;
GRANT SELECT ON v_$statname TO plustrace;
GRANT SELECT ON v_$mystat TO plustrace;
GRANT plustrace TO dba WITH ADMIN OPTION;
GRANT plustrace TO hr;
GRANT SELECT ON v_$sql TO hr;
GRANT SELECT ON v_$sql_plan TO hr;
GRANT SELECT ON v_$sql_plan_statistics_all TO hr;
GRANT SELECT ON v_$session TO hr;
GRANT SELECT ON v_$parameter TO hr; 
```

- 查看first_name是Luis，last_name是Popp的员工的部门、所在部门的平均工资、所在的职业和该员工所在职业的最高工资和最低工资。

查询1：

```SQL

$sqlplus hr/123@localhost/pdborcl

set autotrace on

select a.first_name,a.last_name,b.JOB_TITLE,c.DEPARTMENT_NAME,
(select avg(salary) from EMPLOYEES where JOB_ID=(select JOB_ID from EMPLOYEES where first_name='Luis' and last_name='Popp')) as "avg",b.max_salary,b.min_salary
from EMPLOYEES a,JOBS b,DEPARTMENTS c where a.JOB_ID=b.JOB_ID and a.first_name='Luis' and a.last_name='Popp' 
and a.DEPARTMENT_ID=c.DEPARTMENT_ID;
输出结果：
Predicate Information (identified by operation id):
---------------------------------------------------

   3 - access("JOB_ID"= (SELECT "JOB_ID" FROM "EMPLOYEES" "EMPLOYEES" WHERE
	      "FIRST_NAME"='Luis' AND "LAST_NAME"='Popp'))
   5 - access("LAST_NAME"='Popp' AND "FIRST_NAME"='Luis')
  10 - access("A"."LAST_NAME"='Popp' AND "A"."FIRST_NAME"='Luis')
  12 - access("A"."DEPARTMENT_ID"="C"."DEPARTMENT_ID")
  13 - access("A"."JOB_ID"="B"."JOB_ID")

Note
-----
   - this is an adaptive plan

统计信息
-------------------------------------------------------
	174  recursive calls
	  0  db block gets
	295  consistent gets
	  0  physical reads
	  0  redo size
   1016  bytes sent via SQL*Net to client
	608  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	 15  sorts (memory)
	  0  sorts (disk)
	  1  rows processed


```

![image-20230321185856777](./image-20230321185856777.png)

查询2：

```SQL
set autotrace on

select a.first_name,a.last_name,b.JOB_TITLE,c.DEPARTMENT_NAME,
(select avg(salary) from EMPLOYEES group by job_id having job_id in (select JOB_ID from EMPLOYEES where first_name='Luis' and last_name='Popp')) as "avg",b.max_salary,b.min_salary
from EMPLOYEES a,JOBS b,DEPARTMENTS c where a.JOB_ID=b.JOB_ID and a.first_name='Luis' and a.last_name='Popp' 
and a.DEPARTMENT_ID=c.DEPARTMENT_ID;

输出结果：

Predicate Information (identified by operation id):
---------------------------------------------------

   1 - filter( EXISTS (SELECT 0 FROM "EMPLOYEES" "EMPLOYEES" WHERE "FIRST_NAME"=
'Luis' AND

	      "LAST_NAME"='Popp' AND "JOB_ID"=:B1))
   4 - filter("JOB_ID"=:B1)
   5 - access("LAST_NAME"='Popp' AND "FIRST_NAME"='Luis')
  10 - access("A"."LAST_NAME"='Popp' AND "A"."FIRST_NAME"='Luis')
  12 - access("A"."DEPARTMENT_ID"="C"."DEPARTMENT_ID")
  13 - access("A"."JOB_ID"="B"."JOB_ID")

Note
-----
   - this is an adaptive plan



统计信息
----------------------------------------------------------
	  1  recursive calls
	  0  db block gets
	 51  consistent gets
	  0  physical reads
	  0  redo size
       1016  bytes sent via SQL*Net to client
	608  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	  0  sorts (memory)
	  0  sorts (disk)
	  1  rows processed


```

![image-20230321194104049](./image-20230321194104049.png)



