﻿﻿﻿﻿# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

- 学号：202010414316    姓名：佘尚联

### 实验目的

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

### 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

### 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

### 步骤

#### 创建用户并授权,连接数据库

``` sql
create user ssllp identified by 123456;
grant connect, resource to ssllp;
grant unlimited tablespace  to ssllp;
```

![image-20230525115322103](./pict1.png) 

![image-20230525120229675](pict2.png)  

#### 创建表空间

```
CREATE TABLESPACE Ssllp_SYSTEM
DATAFILE '/home/oracle/app/oracle/oradata/orcl/OracleSystem01.dbf'
SIZE 500M
  4  AUTOEXTEND ON NEXT 100M;

CREATE TABLESPACE Ssllp_DATA
DATAFILE '/home/oracle/app/oracle/oradata/orcl/MyOracleUserdata01.dbf'
SIZE 500M
  4  AUTOEXTEND ON NEXT 100M;
```

![image-20230525120643176](pict3.png) 

##### 表空间结构

| 表空间名称   | 表名称        | 表描述               |
| ------------ | :------------ | -------------------- |
| Ssllp_DATA   | USERS         | 用户信息表           |
| Ssllp_DATA   | MY_ORDERS     | 订单信息表           |
| Ssllp_DATA   | ORDER_DETAILS | 订单详情表           |
| Ssllp_DATA   | PRODUCTS      | 商品信息表           |
| Ssllp_DATA   | SALES_SUMMARY | 销售额表             |
| Ssllp_SYSTEM |               | 存储各个表的相关索引 |

Ssllp_DATA表空间存储数据，Ssllp_SYSTEM表空间存储相关索引

#### 创建相关表

##### 1.商品信息表

```sql
-- 创建商品信息表
CREATE TABLE PRODUCTS (
PRODUCT_ID   NUMBER(10) PRIMARY KEY,
PRODUCT_NAME VARCHAR2(50),
PRICE        NUMBER(10,2),
DESCRIPTION  VARCHAR2(200)
)TABLESPACE Ssllp_DATA;
```

![image-20230525133428455](pict4.png)



##### 2.订单信息表

```sql
-- 创建订单信息表
CREATE TABLE MY_ORDERS (
ORDER_ID   NUMBER(10) PRIMARY KEY,
USER_ID    NUMBER(10),
ORDER_TIME DATE,
STATUS     VARCHAR2(20)
)TABLESPACE Ssllp_DATA;
```

![image-20230525133722971](pict5.png)

##### 3.订单详情表

```sql
-- 创建订单详情表
CREATE TABLE ORDER_DETAILS (
DETAILS_ID NUMBER(10) PRIMARY KEY,
ORDER_ID   NUMBER(10),
PRODUCT_ID NUMBER(10),
QUANTITY   NUMBER(10),
PRICE      NUMBER(10,2)
)TABLESPACE Ssllp_DATA;
```

![image-20230525133924799](pict6.png)

##### 4.用户信息表

```sql
-- 创建用户信息表
CREATE TABLE USERS (
USER_ID   NUMBER(10) PRIMARY KEY,
USERNAME VARCHAR2(50),
PASSWORD VARCHAR2(50),
PHONE    VARCHAR2(20),
EMAIL    VARCHAR2(50)
)TABLESPACE Ssllp_DATA;
```

![image-20230525135244589](pict7.png)

##### 5.销售额表

```sql
CREATE TABLE SALES_SUMMARY(
  PRODUCT_ID NUMBER,
  SELL_DATE  DATE,
  TOTAL_SALE NUMBER(12,2)
)TABLESPACE Ssllp_DATA;

```

##### 6.创建相关索引

```sql
-- 为 PRODUCTS 表创建索引
CREATE INDEX IDX_PRODUCTS_NAME ON PRODUCTS (PRODUCT_NAME) TABLESPACE Ssllp_SYSTEM;
CREATE INDEX IDX_PRODUCTS_PRICE ON PRODUCTS (PRICE) TABLESPACE Ssllp_SYSTEM;

-- 为 MY_ORDERS 表创建索引
CREATE INDEX IDX_MY_ORDERS_USER_ID ON MY_ORDERS (USER_ID) TABLESPACE Ssllp_SYSTEM;
CREATE INDEX IDX_MY_ORDERS_STATUS ON MY_ORDERS (STATUS) TABLESPACE Ssllp_SYSTEM;

-- 为 ORDER_DETAILS 表创建索引
CREATE INDEX IDX_ORDER_DETAILS_ORDER_ID ON ORDER_DETAILS (ORDER_ID) TABLESPACE Ssllp_SYSTEM;
CREATE INDEX IDX_ORDER_DETAILS_PRODUCT_ID ON ORDER_DETAILS (PRODUCT_ID) TABLESPACE Ssllp_SYSTEM;

-- 为 USERS 表创建索引
CREATE INDEX IDX_USERS_USERNAME ON USERS (USERNAME) TABLESPACE Ssllp_SYSTEM;
CREATE INDEX IDX_USERS_EMAIL ON USERS (EMAIL) TABLESPACE Ssllp_SYSTEM;
```



#### 插入填充数据

##### 1.插入用户表

```sql
-- 填充3w条USERS表数据
DECLARE
    v_user_id NUMBER;
BEGIN
    FOR i IN 1..30000 LOOP
        v_user_id := i;
        INSERT INTO users (user_id, username, password, phone, email)
        VALUES (v_user_id, 'User ' || v_user_id, 'password', RPAD('138' || lpad(i, 8, '0'), 20, ' '), 'user' || v_user_id || '@example.com');
    END LOOP;
END;
/
-- 使用PL/SQL语言编写脚本快速生成数据
```

![image-20230525140102803](pict8.png)

##### 2.填充商品信息表

```sql
-- 填充3w条PRODUCTS表数据
DECLARE
    v_product_id NUMBER;
BEGIN
    FOR i IN 1..30000 LOOP
        v_product_id := i;
        INSERT INTO products (product_id, product_name, price, description)
        VALUES (v_product_id, 'Product ' || v_product_id, round(dbms_random.value(5, 1000), 2), RPAD('Description ' || v_product_id, 200, '*'));
    END LOOP;
END;
/
```

![image-20230525140030194](pict9.png)

##### 3.填充订单表

```sql
-- 填充MY_ORDERS表数据
DECLARE
    v_order_id NUMBER;
BEGIN
    FOR i IN 1..30000 LOOP
        v_order_id := i;
        INSERT INTO my_orders (order_id, user_id, order_time, status)
        VALUES (v_order_id, trunc(dbms_random.value(1, 30001)), to_date('2022-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss') + dbms_random.value(1, 365), DECODE(MOD(i, 5), 0, 'Canceled', 1, 'Pending', 'Delivered'));
    END LOOP;
END;
/
```

![image-20230525140437848](pict10.png)

##### 4.填充订单详情表

```sql
-- 填充ORDER_DETAILS表数据
DECLARE
    v_details_id NUMBER;
BEGIN
    FOR i IN 1..30000 LOOP
        v_details_id := i;
        INSERT INTO order_details (details_id, order_id, product_id, quantity, price)
        VALUES (v_details_id, dbms_random.value(1, 30001), dbms_random.value(1, 30001), round(dbms_random.value(1, 10)), round(dbms_random.value(5, 1000), 2));
    END LOOP;
END;
/
```

![image-20230525141121997](pict11.png) 

#### 设计权限及用户分配

##### 创建用户

```sql
-- 创建用户1（普通用户）
CREATE USER USER_1 IDENTIFIED BY password;
-- 创建用户2（管理员用户）
CREATE USER USER_2 IDENTIFIED BY password;
```

##### 赋予权限

```sql
-- 为用户1赋予权限
GRANT SELECT, INSERT ON PRODUCTS TO USER_1;
GRANT SELECT, INSERT ON MY_ORDERS TO USER_1;
GRANT SELECT ON USERS TO USER_1;
GRANT SELECT ON ORDER_DETAILS TO USER_1;
-- 为用户2赋予权限
GRANT SELECT, UPDATE, DELETE ON PRODUCTS TO USER_2;
GRANT SELECT, UPDATE, DELETE ON MY_ORDERS TO USER_2;
GRANT SELECT, UPDATE, DELETE ON USERS TO USER_2;
GRANT SELECT ON ORDER_DETAILS TO USER_2;
```

![image-20230525143406418](pict12.png) 

#### PL/SQL设计

##### PACKAGE_ORDER

```sql
-- 创建程序包 PACKAGE_ORDER
CREATE OR REPLACE PACKAGE PACKAGE_ORDER AS

-- 定义一个存储过程，用于新增订单信息
PROCEDURE ADD_ORDER(P_ORDER_ID   IN NUMBER,
                    P_USER_ID    IN NUMBER,
                    P_ORDER_TIME IN DATE,
                    P_STATUS     IN VARCHAR2);

-- 定义一个存储过程，用于修改订单状态
PROCEDURE UPDATE_ORDER_STATUS(P_ORDER_ID IN NUMBER, P_STATUS IN VARCHAR2);

-- 定义一个存储过程，用于删除订单信息
PROCEDURE DELETE_ORDER(P_ORDER_ID IN NUMBER);

-- 定义一个函数，用于查询订单信息
FUNCTION QUERY_ORDER(P_ORDER_ID IN NUMBER) RETURN SYS_REFCURSOR;

END PACKAGE_ORDER;
/

-- 在程序包中实现存储过程和函数
CREATE OR REPLACE PACKAGE BODY PACKAGE_ORDER AS

-- 实现新增订单信息的存储过程
PROCEDURE ADD_ORDER(P_ORDER_ID   IN NUMBER,
                    P_USER_ID    IN NUMBER,
                    P_ORDER_TIME IN DATE,
                    P_STATUS     IN VARCHAR2) IS
BEGIN
  INSERT INTO MY_ORDERS(ORDER_ID, USER_ID, ORDER_TIME, STATUS)
  VALUES (P_ORDER_ID, P_USER_ID, P_ORDER_TIME, P_STATUS);
END ADD_ORDER;

-- 实现修改订单状态的存储过程
PROCEDURE UPDATE_ORDER_STATUS(P_ORDER_ID IN NUMBER, P_STATUS IN VARCHAR2) IS
BEGIN
  UPDATE MY_ORDERS SET STATUS = P_STATUS WHERE ORDER_ID = P_ORDER_ID;
END UPDATE_ORDER_STATUS;

-- 实现删除订单信息的存储过程
PROCEDURE DELETE_ORDER(P_ORDER_ID IN NUMBER) IS
BEGIN
  DELETE FROM MY_ORDERS WHERE ORDER_ID = P_ORDER_ID;
END DELETE_ORDER;

-- 实现查询订单信息的函数
FUNCTION QUERY_ORDER(P_ORDER_ID IN NUMBER) RETURN SYS_REFCURSOR IS
  C_ORDER SYS_REFCURSOR;
BEGIN
  OPEN C_ORDER FOR
    SELECT O.ORDER_ID, O.USER_ID, U.USERNAME, O.ORDER_TIME, O.STATUS,
           D.PRODUCT_ID, P.PRODUCT_NAME, D.QUANTITY, D.PRICE
      FROM MY_ORDERS O
      LEFT JOIN USERS U ON O.USER_ID = U.USER_ID
      LEFT JOIN ORDER_DETAILS D ON O.ORDER_ID = D.ORDER_ID
      LEFT JOIN PRODUCTS P ON D.PRODUCT_ID = P.PRODUCT_ID
     WHERE O.ORDER_ID = P_ORDER_ID;
  RETURN C_ORDER;
END QUERY_ORDER;

END PACKAGE_ORDER;
/

```

![image-20230525150509311](pict13.png) 

​     在程序包 PACKAGE_ORDER 中，我定义了四个对象：ADD_ORDER 存储过程、UPDATE_ORDER_STATUS 存储过程、DELETE_ORDER 存储过程和 QUERY_ORDER 函数。其中，ADD_ORDER 存储过程用于新增订单信息，UPDATE_ORDER_STATUS 存储过程用于修改订单状态，DELETE_ORDER 存储过程用于删除订单信息，QUERY_ORDER 函数用于查询订单信息。通过以上PL/SQL代码的实现，可以通过程序包 PACKAGE_ORDER 中的存储过程和函数来操作数据库中的订单信息，并且可以根据具体业务逻辑，进一步扩展实现更多的存储过程和函数。

##### PACKAGE_SALES

```sql
-- 创建程序包 PACKAGE_SALES
CREATE OR REPLACE PACKAGE PACKAGE_SALES AS

-- 定义一个存储过程，用于计算某一产品在过去一个月中的总销售额，并插入到 SALES_SUMMARY 表中。
PROCEDURE CALCULATE_MONTHLY_SALES(P_PRODUCT_ID IN NUMBER);

-- 定义一个存储过程，用于获取某一时间段内的所有订单，并对这些订单进行统计分析，返回各个用户的订单总数和销售额。
PROCEDURE GET_ORDER_STATISTICS(P_START_DATE IN DATE, P_END_DATE IN DATE, P_USER_ID OUT NUMBER, P_TOTAL_SALES OUT NUMBER);

END PACKAGE_SALES;
/

-- 在程序包中实现存储过程
CREATE OR REPLACE PACKAGE BODY PACKAGE_SALES AS

-- 实现 CALCULATE_MONTHLY_SALES 存储过程
PROCEDURE CALCULATE_MONTHLY_SALES(P_PRODUCT_ID IN NUMBER) IS
  V_START_DATE DATE := ADD_MONTHS(TRUNC(SYSDATE,'MM'), -1);
  V_END_DATE   DATE := LAST_DAY(ADD_MONTHS(TRUNC(SYSDATE,'MM'), -1));
BEGIN
  INSERT INTO SALES_SUMMARY(PRODUCT_ID, SELL_DATE, TOTAL_SALE)
  SELECT D.PRODUCT_ID, TRUNC(O.ORDER_TIME, 'MM') SELL_DATE, SUM(D.PRICE * D.QUANTITY) TOTAL_SALE
    FROM MY_ORDERS O
    JOIN ORDER_DETAILS D ON O.ORDER_ID = D.ORDER_ID
   WHERE D.PRODUCT_ID = P_PRODUCT_ID
     AND O.ORDER_TIME BETWEEN V_START_DATE AND V_END_DATE
   GROUP BY D.PRODUCT_ID, TRUNC(O.ORDER_TIME, 'MM');
END CALCULATE_MONTHLY_SALES;

-- 实现 GET_ORDER_STATISTICS 存储过程
PROCEDURE GET_ORDER_STATISTICS(P_START_DATE IN DATE, P_END_DATE IN DATE, P_USER_ID OUT NUMBER, P_TOTAL_SALES OUT NUMBER) IS
BEGIN
  SELECT COUNT(DISTINCT O.USER_ID) INTO P_USER_ID
    FROM MY_ORDERS O
   WHERE O.ORDER_TIME BETWEEN P_START_DATE AND P_END_DATE;

  SELECT SUM(D.PRICE * D.QUANTITY) INTO P_TOTAL_SALES
    FROM MY_ORDERS O
    JOIN ORDER_DETAILS D ON O.ORDER_ID = D.ORDER_ID
   WHERE O.ORDER_TIME BETWEEN P_START_DATE AND P_END_DATE;
END GET_ORDER_STATISTICS;

END PACKAGE_SALES;
/

```

![image-20230525152607718](pict14.png)

​    在 PACKAGE_SALES 中实现的 CALCULATE_MONTHLY_SALES 存储过程用于计算某一产品在过去一个月中的总销售额，并将结果插入到 SALES_SUMMARY 表中。此存储过程将首先获取上个月的起始时间、结束时间，然后根据指定的 product_id 来查询该产品在上个月中的销售情况，并将结果插入到 SALES_SUMMARY 表中。

​    另外，PACKAGE_SALES 中还实现了 GET_ORDER_STATISTICS 存储过程，用于获取某一时间段内的所有订单，并对这些订单进行统计分析，返回各个用户的订单总数和销售额。其中，该存储过程将首先根据指定的时间段查询出符合条件的订单信息，并分别计算出不同用户的订单总数以及总销售额，最后将这些结果通过 out 参数返回调用者。

#### 备份方案

##### 1.增量导出／导入方案

利用Export可将数据从数据库中提取出来，利用Import则可将提取出来的数据送回到Oracle数据库中去。

> 增量导出是一种常用的数据备份方法，它只能对整个数据库来实施，并且必须作为SYSTEM来导出。在进行此种导出时，系统不要求回答任何问题。导出文件名缺省为export.dmp，如果不希望自己的输出文件定名为export.dmp，必须在命令行中指出要用的文件名。
> 增量导出包括三种类型：
> （１）、“完全”增量导出（Complete）
> 即备份三个数据库，比如：
> exp system/manager inctype=complete file=‘/home/oracle/app/oracle/oradata/orcl/demo_user.dmp’
> （２）、“增量型”增量导出
> 备份上一次备份后改变的数据，比如：
> exp system/manager inctype=incremental file=040731.dmp

##### 2.热备份--RMAN 备份

1. 创建一个存储备份文件的目录。

```
sqlCopy CodeCREATE OR REPLACE DIRECTORY BACKUP_DIR AS '/home/oracle/app/oracle/oradata/orcl/u01/backup';
```

   2.使用 RMAN 工具进行数据库备份，并将备份文件保存到指定目录中。

- 完整备份

```
sqlCopy CodeRUN {
  ALLOCATE CHANNEL c1 DEVICE TYPE disk FORMAT '/home/oracle/app/oracle/oradata/orcl/u01/backup/full_%d_%Y%m%d_%s.bak';
  BACKUP DATABASE PLUS ARCHIVELOG;
  RELEASE CHANNEL c1;
}
```

上述语句将使用 RMAN 工具对整个数据库进行备份，包括归档日志文件。备份文件将以 full_数据库名_备份日期_备份序号.bak 的格式保存在 BACKUP_DIR 目录下。

- 增量备份

```
sqlCopy CodeRUN{
  ALLOCATE CHANNEL c1 DEVICE TYPE disk FORMAT '/home/oracle/app/oracle/oradata/orcl/u01/backup/incr_%d_%Y%m%d_%s.bak';
  BACKUP INCREMENTAL LEVEL 1 DATABASE PLUS ARCHIVELOG;
  RELEASE CHANNEL c1;
}
```

上述语句将使用 RMAN 工具对增量数据进行备份，并将备份文件保存在 BACKUP_DIR 目录下。

   3.检查备份文件的完整性和可恢复性。

```
sqlCopy CodeRUN {
  ALLOCATE CHANNEL c1 DEVICE TYPE disk;
  RESTORE DATABASE VALIDATE CHECK LOGICAL;
  RELEASE CHANNEL c1;
}
```

上述语句将检查备份文件的完整性和可恢复性，并输出检查结果。如果备份文件出现问题，需要及时重新执行备份操作。

   4.定时运行备份任务。

使用 Unix cron 或 Windows 计划任务定时运行备份任务，确保数据库的备份工作自动化和及时性。

### 总结

​    在本次实验中，我进一步加深了对oracle数据库的了解和使用，根据题目要求，进行了用户创建、数据库连接、创建表空间、创建相关表，创建了用户表（USERS）商品信息表 (PRODUCTS)，订单详情表(ORDER_DETAIL)，订单表 (MY_ORDER)，销售额表(SUM_SALARY)，之后使用PL/SQL编写脚本对数据表进行快速的填充，用以模拟真实的使用场景，之后进行了权限的设计和用户的分配，两个用户有着对不同表的管理权限，之后进行了存储过程和函数的设计，创建了PACKAGE_ORDER程序包，其中是对订单信息的增删改查的函数，之后创建了PACKAGE_SALES程序包， 其中的CALCULATE_MONTHLY_SALES 存储过程用于计算某一产品在过去一个月中的总销售额，并将结果插入到 SALES_SUMMARY 表中。还实现了 GET_ORDER_STATISTICS 存储过程，用于获取某一时间段内的所有订单，并对这些订单进行统计分析，返回各个用户的订单总数和销售额，之后准备了数据库的备份方案。整个数据库的设计依旧不是很完善，表之间的关联性依旧较弱，业务逻辑较少，会在将来的学习中对数据库进行进一步的完善，该实验加深了我对课程的理解和对oracle的应用。

